package main

import (
	"github.com/spf13/viper"

	"strings"
	"time"
)

type serverConfig struct {
	Listen    string
	ModSecret string
	BaseURL   string
}

type bigBlueButtonConfig struct {
	API          string
	Secret       string
	RoomSize     uint32
	SyncInterval time.Duration
}

type config struct {
	Server        *serverConfig        `toml:"server"`
	BigBlueButton *bigBlueButtonConfig `toml:"bigbluebutton"`
}

func loadConfig() *config {
	viper.SetConfigName("bigbluedispatch.conf")
	viper.SetConfigType("toml")
	viper.AddConfigPath("./etc")
	viper.AddConfigPath("/etc")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	// Parse sync interval
	syncInterval := time.Duration(viper.GetInt(
		"bigbluebutton.sync_interval")) * time.Second
	if syncInterval == 0 {
		panic("SyncInterval not configured!")
	}

	// Sanitize base url
	baseURL := viper.GetString("server.base_url")
	if strings.HasSuffix(baseURL, "/") {
		baseURL = baseURL[:len(baseURL)-1]
	}

	// Build config
	c := &config{
		Server: &serverConfig{
			Listen:    viper.GetString("server.listen"),
			ModSecret: viper.GetString("server.mod_secret"),
			BaseURL:   baseURL,
		},
		BigBlueButton: &bigBlueButtonConfig{
			API:          viper.GetString("bigbluebutton.api"),
			Secret:       viper.GetString("bigbluebutton.secret"),
			RoomSize:     uint32(viper.GetInt("bigbluebutton.room_size")),
			SyncInterval: syncInterval,
		},
	}

	return c
}
