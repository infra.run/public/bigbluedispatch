package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	fmt.Println("BigBlueDispatch\t\t\t\t\tv.0.1.0")

	// Load configuration and initialize
	cfg := loadConfig()
	fmt.Println("Using BBB API @", cfg.BigBlueButton.API)
	fmt.Println("Room Size:", cfg.BigBlueButton.RoomSize)
	fmt.Println("Sync Interval:", cfg.BigBlueButton.SyncInterval)

	// BBB State
	bbb := NewBigBlueButton(cfg.BigBlueButton)
	go bbb.Start()

	// HTTP Interface
	ifHTTP := NewHTTPInterface(cfg.Server, bbb)
	ifHTTP.Start()
}
