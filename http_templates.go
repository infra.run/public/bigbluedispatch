package main

import (
	"html/template"
)

func compileTmplStatus() *template.Template {
	tmpl := `
        <html>
            <h1>Status</h1>
            <table width="100%" border="2">
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Participants</th>
                <th>Mods</th>
                <th>Running</th>
                <th>Sync</th>
              </tr>
              {{range .meetings}}
                <tr>
                    <td>{{.MeetingID}}</td>
                    <td>{{.MeetingName}}</td>
                    <td>{{.ParticipantCount}}</td>
                    <td>{{.ModeratorCount}}</td>
                    <td>{{.Running}}</td>
                    <td>{{.SyncedAt}}</td>
                </tr>
              {{end}}
            </table>
        </html>
    `

	t, _ := template.New("status").Parse(tmpl)
	return t
}
