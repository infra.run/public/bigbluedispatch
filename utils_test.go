package main

import (
	"testing"
)

func TestSecureStringsEq(t *testing.T) {
	if SecureStringsEq("foo", "foo") == false {
		t.Error("foo == foo")
	}
	if SecureStringsEq("foo", "bar") == true {
		t.Error("foo != bar")
	}

	if SecureStringsEq("foo000000", "bar") == true {
		t.Error("foo000000 != bar")
	}

}
