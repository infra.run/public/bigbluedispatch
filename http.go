package main

import (
	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"html/template"
	"net/http"

	"bytes"
	"fmt"
	"log"
	"time"
)

// HTTPInterface provides a http server where
// the clients are dispatched.
//
type HTTPInterface struct {
	echo   *echo.Echo
	config *serverConfig
	bbb    *BigBlueButton

	templates map[string]*template.Template
}

// NewHTTPInterface will create a new instance
func NewHTTPInterface(
	config *serverConfig,
	bbb *BigBlueButton,
) *HTTPInterface {

	// Setup and configure echo framework
	e := echo.New()
	e.HideBanner = true

	// Middlewares
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Prometheus Middleware
	// Find it under /metrics
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)

	// Templates
	templates := map[string]*template.Template{
		"status": compileTmplStatus(),
	}

	iface := &HTTPInterface{
		echo:      e,
		config:    config,
		bbb:       bbb,
		templates: templates,
	}

	// Register Routes
	e.GET("/", iface.httpIndex)
	e.GET("/status", iface.httpStatus)
	e.GET("/join/:topic", iface.httpJoinMeeting)

	return iface
}

// Start the HTTP interface and listen
func (iface *HTTPInterface) Start() {
	// Start the HTTP service.
	log.Fatal(iface.echo.Start(iface.config.Listen))
}

//
// Request Handlers
//

// Index / Root Handler
func (iface *HTTPInterface) httpIndex(c echo.Context) error {
	return c.HTML(
		http.StatusOK,
		"<h1>Big Blue Dispatch! v1.1.0</h1>")

}

// Status Handler
func (iface *HTTPInterface) httpStatus(c echo.Context) error {
	var body bytes.Buffer
	iface.templates["status"].Execute(&body, Params{
		"meetings": iface.bbb.meetings,
	})
	return c.HTML(http.StatusOK, body.String())
}

// Join / Dispatch Handler
func (iface *HTTPInterface) httpJoinMeeting(c echo.Context) error {
	var dispatcherURL string
	topic := c.Param("topic")
	role := c.QueryParam("role")
	if role == roleModerator {
		// Validate secret
		secret := c.QueryParam("secret")

		if !SecureStringsEq(secret, iface.config.ModSecret) {
			// 403!
			return echo.NewHTTPError(
				http.StatusForbidden,
				"Your moderator secret is invalid")
		}

		dispatcherURL = fmt.Sprintf(
			"%s/join/%s?secret=%s&role=moderator",
			iface.config.BaseURL, topic, secret)

	} else {
		role = roleUser
		dispatcherURL = fmt.Sprintf(
			"%s/join/%s",
			iface.config.BaseURL, topic)
	}

	// Great - Let's try to join the room,
	// When it fails, don't give up.
	var (
		roomURL string
		err     error
	)
	for i := 0; i < 10; i++ {
		roomURL, err = iface.bbb.DispatchClient(topic, role, dispatcherURL)
		if err != nil {
			log.Println("ERROR joining room:", err)
			time.Sleep(250 * time.Millisecond)
		} else {
			break // It worked!
		}
	}

	// Redirect to BBB session
	return c.Redirect(http.StatusFound, roomURL)

}
