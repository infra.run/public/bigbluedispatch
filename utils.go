package main

import (
	"crypto/subtle"
)

// SecureStringsEq compares two strings in constant time
// to prevent timing attacks.
func SecureStringsEq(s1, s2 string) bool {
	// We use the constant time compare with byte slices
	if subtle.ConstantTimeCompare([]byte(s1), []byte(s2)) == 1 {
		return true
	}

	return false
}
