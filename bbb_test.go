package main

import (
	"testing"
)

func TestParamsEncode(t *testing.T) {
	p := Params{
		"foo": 23,
		"bar": true,
		"bam": "plaintext",
	}

	qry := p.Encode()
	t.Log(qry)
	// TODO add assertions
}

func TestBBBEndpointURL(t *testing.T) {
	bbb := &BigBlueButtonClient{
		config: &bigBlueButtonConfig{
			API:    "https://foo/bar/api",
			Secret: "fooooo",
		},
	}

	url := bbb.endpointURL("createRoom", Params{"my": "params"})
	t.Log(url)
	// TODO add assertions
}

func TestBBBJoinMeetingURL(t *testing.T) {
	bbb := &BigBlueButtonClient{
		config: &bigBlueButtonConfig{
			API:    "https://foo/bar/api",
			Secret: "fooooo",
		},
	}
	url := bbb.JoinMeetingURL("meeeting-id", "name", "pass")
	t.Log(url)
}

func TestMeetingsCollectionAdd(t *testing.T) {
	m := &Meeting{}
	mc := MeetingsCollection{}

	mc = mc.Add(m)
	if len(mc) != 1 {
		t.Error("Expected len 1")
	}

	mc = mc.Add(m)
	if len(mc) != 2 {
		t.Error("Expected len 2")
	}
}

func TestMeetingsCollectionRemove(t *testing.T) {
	m1 := &Meeting{}
	m2 := &Meeting{}
	m3 := &Meeting{}

	mc := MeetingsCollection{m1, m2, m3}

	mc = mc.Remove(m2)
	if len(mc) != 2 {
		t.Error("Expected len 2")
	}
	if mc[0] != m1 {
		t.Error("Expected m1 to be in collection")
	}
	if mc[1] != m3 {
		t.Error("Expected m3 to be in collection")
	}

	mc = mc.Remove(m1)
	if len(mc) != 1 {
		t.Error("Expected removal")
	}

	mc = mc.Remove(m1)
	if len(mc) != 1 {
		t.Error("Expected no removal")
	}

}

func TestMeetingUpdate(t *testing.T) {
	m1 := &Meeting{
		ParticipantCount: 3,
		ModeratorCount:   1,
	}
	m2 := &Meeting{
		ParticipantCount: 10,
		ModeratorCount:   0,
	}

	m1.Update(m2)
	if m1.ParticipantCount != 10 {
		t.Error("Expected PC 10")
	}
	if m1.ModeratorCount != 0 {
		t.Error("Expected MC 0")
	}
}
