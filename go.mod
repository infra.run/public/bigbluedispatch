module gitlab.com/infra.run/public/bigbluedispatch

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/labstack/echo-contrib v0.9.0
	github.com/labstack/echo/v4 v4.1.6
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/prometheus/client_golang v1.1.0
	github.com/spf13/viper v1.7.0
	github.com/valyala/fasttemplate v1.1.0 // indirect
)
