package main

import (
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"crypto/sha1"
	"encoding/hex"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	netUrl "net/url"
)

const (
	roleUser      = "user"
	roleModerator = "moderator"
)

/*
 Datastructures
*/

// Params for the BBB API
type Params map[string]interface{}

// Encode query parameters
func (p Params) Encode() string {
	var q []string
	for k, v := range p {
		vStr := netUrl.QueryEscape(fmt.Sprintf("%v", v))
		q = append(q, fmt.Sprintf("%s=%s", k, vStr))
	}
	return strings.Join(q, "&")
}

// Attendees collection
type Attendees struct {
	XMLName   xml.Name   `xml:"attendees"`
	Attendees []Attendee `xml:"attendee"`
}

// Attendee of a meeting
type Attendee struct {
	XMLName         xml.Name `xml:"attendee"`
	UserID          string   `xml:"userID"`
	FullName        string   `xml:"fullName"`
	IsPresenter     string   `xml:"isPresenter"`
	IsListeningOnly string   `xml:"isListeningOnly"`
	HasJoinedVoice  string   `xml:"hasJoinedVoice"`
	HasVideo        string   `xml:"hasVideo"`
	ClientType      string   `xml:"clientType"`
}

// Metadata about the BBB instance
type Metadata struct {
	XMLName             xml.Name `xml:"metadata"`
	BBBOriginVersion    string   `xml:"bbb-origin-version"`
	BBBOriginServerName string   `xml:"bbb-origin-server-name"`
	BBBOrigin           string   `xml:"bbb-origin"`
	GlListed            string   `xml:"gl-listed"`
}

// Meeting information
type Meeting struct {
	XMLName               xml.Name  `xml:"meeting"`
	MeetingName           string    `xml:"meetingName"`
	MeetingID             string    `xml:"meetingID"`
	InternalMeetingID     string    `xml:"internalMeetingID"`
	CreateTime            uint64    `xml:"createTime"`
	CreateDate            string    `xml:"createDate"`
	VoiceBridge           string    `xml:"voiceBridge"`
	DialNumber            string    `xml:"dialNumber"`
	AttendeePW            string    `xml:"attendeePW"`
	ModeratorPW           string    `xml:"moderatorPW"`
	Running               string    `xml:"running"`
	Duration              int16     `xml:"duration"`
	Recording             string    `xml:"recording"`
	HasBeenForciblyEnded  string    `xml:"hasBeenForciblyEnded"`
	StartTime             uint64    `xml:"startTime"`
	EndTime               uint64    `xml:"endTime"`
	ParticipantCount      uint32    `xml:"participantCount"`
	ListenerCount         uint32    `xml:"listenerCount"`
	VoiceParticipantCount uint32    `xml:"voiceParticipantCount"`
	VideoCount            uint32    `xml:"videoCount"`
	MaxUsers              uint32    `xml:"maxUsers"`
	ModeratorCount        uint32    `xml:"moderatorCount"`
	Attendees             Attendees `xml:"attendees"`
	Metadata              Metadata  `xml:"metadata"`
	IsBreakout            string    `xml:"isBreakout"`

	SyncedAt time.Time
	Mux      sync.Mutex
}

// NextUsername for a role
func (m *Meeting) NextUsername(role string) string {
	if role == roleModerator {
		return "Moderator" // Todo add enumeration (maybe)
	}

	// Get next username in list.
	nextIdx := (int(m.ParticipantCount) + 1) % len(Usernames)
	return Usernames[nextIdx]
}

// Password for a role
func (m *Meeting) Password(role string) string {
	if role == roleModerator {
		return m.ModeratorPW
	}

	return m.AttendeePW
}

// Update meeting fields
func (m *Meeting) Update(meeting *Meeting) {
	m.Mux.Lock()
	defer m.Mux.Unlock()

	// This is kind of ugly but does not warrent including
	// a new dependecy like `mergo`
	m.MeetingName = meeting.MeetingName
	m.InternalMeetingID = meeting.InternalMeetingID
	m.CreateTime = meeting.CreateTime
	m.CreateDate = meeting.CreateDate
	m.VoiceBridge = meeting.VoiceBridge
	m.DialNumber = meeting.DialNumber
	m.AttendeePW = meeting.AttendeePW
	m.ModeratorPW = meeting.ModeratorPW
	m.Running = meeting.Running
	m.Recording = meeting.Recording
	m.Duration = meeting.Duration
	m.HasBeenForciblyEnded = meeting.HasBeenForciblyEnded
	m.StartTime = meeting.StartTime
	m.EndTime = meeting.EndTime
	m.ParticipantCount = meeting.ParticipantCount // This is why we are here
	m.ListenerCount = meeting.ListenerCount
	m.VoiceParticipantCount = meeting.VoiceParticipantCount
	m.VideoCount = meeting.VideoCount
	m.MaxUsers = meeting.MaxUsers
	m.ModeratorCount = meeting.ModeratorCount
	m.Attendees = meeting.Attendees
	m.Metadata = meeting.Metadata
	m.IsBreakout = meeting.IsBreakout

	// Update sync timestamp
	m.SyncedAt = time.Now()
}

// MeetingInfo contains getMeetingInfo details
type MeetingInfo struct {
	Meeting
	XMLName xml.Name `xml:"response"`
}

func (m *Meeting) String() string {
	return fmt.Sprintf(
		"[Meeting id: %v, pc: %v, mc: %v, running: %v]",
		m.MeetingID, m.ParticipantCount, m.ModeratorCount, m.Running,
	)
}

// MeetingsCollection is a list of Meetings with
// additional helper methods for lookup and removal
type MeetingsCollection []*Meeting

// Add meeting to collection
func (mc MeetingsCollection) Add(meeting *Meeting) MeetingsCollection {
	// This is kind of trivial
	return append(mc, meeting)
}

// Remove meeting from collection
func (mc MeetingsCollection) Remove(meeting *Meeting) MeetingsCollection {
	// Find meeting index
	idx := -1
	for i, m := range mc {
		if m == meeting {
			idx = i
			break
		}
	}
	// Not found. We ignore this
	if idx == -1 {
		return mc
	}

	// Splice at index
	return append(mc[:idx], mc[idx+1:]...)
}

// FindByID retrieves the Meeting which matches the MeetingID in O(N)
// if the meeting is not found the result is nil.
func (mc MeetingsCollection) FindByID(meetingID string) *Meeting {
	for _, m := range mc {
		if m.MeetingID == meetingID {
			return m
		}
	}
	return nil
}

// Meetings container
type Meetings struct {
	XMLName  xml.Name           `xml:"meetings"`
	Meetings MeetingsCollection `xml:"meeting"`
}

// Response is a Big Blue Button API response
type Response struct {
	XMLName              xml.Name `xml:"response"`
	Returncode           string   `xml:"returncode"`
	Meetings             Meetings `xml:"meetings"`
	MeetingID            string   `xml:"meetingID"`
	InternalMeetingID    string   `xml:"internalMeetingID"`
	ParentMeetingID      string   `xml:"parentMeetingID"`
	AttendeePW           string   `xml:"attendeePW"`
	ModeratorPW          string   `xml:"moderatorPW"`
	CreateTime           string   `xml:"createTime"`
	VoiceBridge          string   `xml:"voiceBridge"`
	DialNumber           string   `xml:"dialNumber"`
	CreateDate           string   `xml:"createDate"`
	HasUserJoined        string   `xml:"hasUserJoined"`
	Duration             string   `xml:"duration"`
	HasBeenForciblyEnded string   `xml:"hasBeenForciblyEnded"`
	MessageKey           string   `xml:"messageKey"`
	Message              string   `xml:"message"`
	AuthToken            string   `xml:"auth_token"`
	SessionToken         string   `xml:"session_token"`
	URL                  string   `xml:"url"`
}

// BigBlueButtonClient API
type BigBlueButtonClient struct {
	config       *bigBlueButtonConfig
	httpRequests *prometheus.CounterVec
	httpDuration *prometheus.HistogramVec
}

// NewBigBlueButtonClient creates a new instance with a config
func NewBigBlueButtonClient(config *bigBlueButtonConfig) *BigBlueButtonClient {
	return &BigBlueButtonClient{
		config: config,
		httpRequests: promauto.NewCounterVec(prometheus.CounterOpts{
			Namespace: "bbb",
			Subsystem: "api",
			Name:      "requests_total",
			Help:      "Number of HTTP operations against the BBB API",
		}, []string{"status", "method", "url"}),
		httpDuration: promauto.NewHistogramVec(prometheus.HistogramOpts{
			Namespace: "bbb",
			Subsystem: "api",
			Name:      "requests_duration_seconds",
			Help:      "Spend time by performing a BBB API call",
			Buckets: []float64{
				0.0005,
				0.001, // 1ms
				0.002,
				0.005,
				0.01, // 10ms
				0.02,
				0.05,
				0.1, // 100 ms
				0.2,
				0.5,
				1.0, // 1s
				2.0,
				5.0,
				10.0, // 10s
				15.0,
				20.0,
				30.0,
			},
		}, []string{"method", "url"}),
	}
}

func (bbb *BigBlueButtonClient) endpointURL(endpoint string, params Params) string {
	paramsQry := params.Encode()

	// Calculate checksum with server secret
	// Basically sign the endpoint + params
	mac := endpoint + paramsQry + bbb.config.Secret
	hash := sha1.New()
	hash.Write([]byte(mac))
	checksum := hex.EncodeToString(hash.Sum(nil))

	// In case the configuration does not end in a trailing slash,
	// append it when needed.
	apiBase := bbb.config.API
	if !strings.HasSuffix(apiBase, "/") {
		apiBase += "/"
	}

	// Build complete url
	return apiBase + endpoint + "?" + paramsQry + "&checksum=" + checksum
}

// Run query against the API
func (bbb *BigBlueButtonClient) query(url string) ([]byte, error) {
	// Strip query from url for metrics
	metricsURL := url[:strings.IndexRune(url, '?')]

	c := &http.Client{
		Timeout: 60 * time.Second,
	}
	timer := prometheus.NewTimer(
		bbb.httpDuration.WithLabelValues("get", metricsURL))
	res, err := c.Get(url)
	timer.ObserveDuration()
	if err != nil {
		// observe errors as status code -1
		bbb.httpRequests.WithLabelValues("-1", "get", metricsURL)
		return nil, err
	}
	// TODO need to normalize HTTP status to 2xx, 3xx, etc?
	bbb.httpRequests.WithLabelValues(
		strconv.Itoa(res.StatusCode), "get", metricsURL)

	// Check response status
	if res.StatusCode >= 400 {
		return nil, fmt.Errorf(
			"received error status code %d from server", res.StatusCode)
	}

	// Load response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

// GetMeetings will return all meetings from a big blue button instance
// The result is cached and refreshed after the TTL expired.
// If it failes, an error will be returned
func (bbb *BigBlueButtonClient) GetMeetings() (MeetingsCollection, error) {
	res, err := bbb.query(bbb.endpointURL("getMeetings", Params{
		"random": rand.Int(),
	}))
	if err != nil {
		return nil, err
	}

	// Decode response body
	var response Response
	if err := xml.Unmarshal(res, &response); err != nil {
		return nil, err
	}

	if response.Returncode != "SUCCESS" {
		return nil, fmt.Errorf(
			"getMeetings not successful: %s",
			response.Message)
	}

	// Set last sync timestamp
	meetings := response.Meetings.Meetings
	now := time.Now()
	for _, m := range meetings {
		m.SyncedAt = now
	}

	return response.Meetings.Meetings, nil
}

// GetMeeting retrieves meeting details for
// a known meeting id.
func (bbb *BigBlueButtonClient) GetMeeting(id string) (*Meeting, error) {
	res, err := bbb.query(bbb.endpointURL("getMeetingInfo", Params{
		"meetingID": id,
	}))
	if err != nil {
		return nil, err
	}
	// Decode response
	var response Response
	if err := xml.Unmarshal(res, &response); err != nil {
		return nil, err
	}
	if response.Returncode != "SUCCESS" {
		return nil, fmt.Errorf("read not successful: %s", response.Returncode)
	}

	// Decode meeting details
	var meeting MeetingInfo
	if err := xml.Unmarshal(res, &meeting); err != nil {
		return nil, err
	}

	m := &meeting.Meeting
	m.SyncedAt = time.Now()

	return m, nil
}

// CreateMeeting will create a new room at our instance
// Note that creating rooms for a topic is
func (bbb *BigBlueButtonClient) CreateMeeting(
	name string, joinURL string,
) (*Meeting, error) {

	// We create our meeting right here
	meeting := &Meeting{
		MeetingID:   uuid.New().String(),
		AttendeePW:  uuid.New().String(),
		ModeratorPW: uuid.New().String(),
		MeetingName: fmt.Sprintf("%s - %s", name, uuid.New().String()),
		Running:     "false",
		SyncedAt:    time.Now(),
	}

	// QUICKFIX: Remove moderator credentials from the room
	qIdx := strings.IndexRune(joinURL, '?')
	if qIdx >= 0 {
		joinURL = joinURL[:qIdx]
	}

	params := Params{
		"name":        meeting.MeetingName,
		"meetingID":   meeting.MeetingID,
		"attendeePW":  meeting.AttendeePW,
		"moderatorPW": meeting.ModeratorPW,

		"welcome": "<h1>Herzlich Willkommen!</h1>" +
			"<p>Bitte haben einen Moment Geduld, weitere Teilnehmer*innen werden dem Raum zugewiesen.</p>" +
			fmt.Sprintf("<p>Falls es doch zu lange dauern sollte: <a target='_self' href=\"%s\">Raumzuteilung erneut starten ...</a></p>", joinURL) +
			"<p><a target='_blank' href='https://digitalitaet20.de/pages/datenschutz/index.html'>Datenschutzerklärung</a></p><hr/>",

		"maxParticipants":        20,
		"record":                 "false",
		"moderatorOnlyMessage":   "<p>Liebe Moderator*in, bitte denke an das Sichern des Chats und der Notizen<p/>",
		"bannerText":             "#schuleneudenken",
		"allowModsToUnmuteUsers": "true",
	}

	// Make API request
	res, err := bbb.query(bbb.endpointURL("create", params))
	if err != nil {
		return nil, err
	}
	var response Response
	if err := xml.Unmarshal(res, &response); err != nil {
		return nil, err
	}
	if response.Returncode != "SUCCESS" {
		return nil, fmt.Errorf(
			"create not successful: %s", response.Returncode)
	}

	// Success - Have fun with the new meeting!
	return meeting, nil
}

// JoinMeetingURL will produce the URL for redirecting the
// joining client. No "API" call will be made by the server.
func (bbb *BigBlueButtonClient) JoinMeetingURL(
	meetingID string,
	name string,
	password string,
) string {
	params := Params{
		"meetingID": meetingID,
		"fullName":  name,
		"password":  password,
	}
	return bbb.endpointURL("join", params)
}

// BigBlueButton State Management

// BigBlueButton State Service
type BigBlueButton struct {
	client *BigBlueButtonClient
	config *bigBlueButtonConfig

	meetings    MeetingsCollection
	meetingsMux sync.Mutex
}

// NewBigBlueButton will initialize the service with a config
func NewBigBlueButton(
	config *bigBlueButtonConfig,
) *BigBlueButton {
	client := NewBigBlueButtonClient(config)
	bbb := &BigBlueButton{
		client: client,
		config: config,
	}

	log.Println("Initializing BigBlueButton service...")
	err := bbb.syncInitialState()
	if err != nil {
		panic(err)
	}

	return bbb
}

// Start the big blue button state managment service
func (bbb *BigBlueButton) Start() error {
	log.Println("Starting BigBlueButton State Management Service...")

	// Sync state with bbb
	go func(bbb *BigBlueButton) {
		// Run every minute or so...
		// TODO: Make configurable
		for {
			time.Sleep(bbb.config.SyncInterval)
			err := bbb.sync()
			if err != nil {
				log.Println("State sync failed:", err)
			}
		}
	}(bbb)

	return nil
}

// Sync the server state
func (bbb *BigBlueButton) syncInitialState() error {
	log.Println("Starting state sync with BBB instance")

	meetings, err := bbb.client.GetMeetings()
	if err != nil {
		log.Println("State sync with BBB instance failed:", err)
		return err
	}

	// Update meetings
	bbb.meetingsMux.Lock()
	defer bbb.meetingsMux.Unlock()

	bbb.meetings = meetings

	log.Println("State synced with BBB instance")

	return nil
}

// Synchronize state: Fetch meeting details when
// last sync is longer than n minutes ago.
//
// Remove stale meetings.
//
// TODO: This has potential for optimization.
//       Current runtime is O(N^2)
//
func (bbb *BigBlueButton) sync() error {
	log.Println("Syncing state with BBB instance...")

	bbb.meetingsMux.Lock()
	defer bbb.meetingsMux.Unlock()

	// Retrieve state from server and update
	// local meetings.
	//
	//  - The meeting exists locally but not remote:
	//     - If the meeting.SyncedAt is longer ago
	//       than a threshold (30 s? a minute?) TODO!
	//         -> Remove from list
	//     - Otherwise: Ignore for now
	//  - The meeting exists locally and remote:
	//     - If the meeting.SyncedAt is longer ago than T
	//         -> Update / Merge
	//     - Otherwise: Ignore for now
	bbbMeetings, err := bbb.client.GetMeetings()
	if err != nil {
		return err
	}

	now := time.Now()
	syncThreshold := 1 * time.Minute

	for _, rMeeting := range bbbMeetings {
		lMeeting := bbb.meetings.FindByID(rMeeting.MeetingID)
		// The meeting does not exist locally
		if lMeeting == nil {
			bbb.meetings = bbb.meetings.Add(rMeeting)
			continue
		}

		// The meeting exists locally, so we need to check the
		// last sync time and update it
		if now.Sub(lMeeting.SyncedAt) > syncThreshold {
			lMeeting.Update(rMeeting)
		}
	}

	// Find meetings that are locally present but not remote
	for _, lMeeting := range bbb.meetings {
		rMeeting := bbbMeetings.FindByID(lMeeting.MeetingID)
		if rMeeting == nil {
			// Check timeout threshold. If last sync is longer ago
			// than 60 seconds remove the meeting.
			if now.Sub(lMeeting.SyncedAt) > syncThreshold {
				bbb.meetings = bbb.meetings.Remove(lMeeting)
			}
		}
	}

	log.Println("State synced with BBB instance.")

	return nil
}

// FindMeetingSlot will search for a free slot in
// the list of our meetings.
func (bbb *BigBlueButton) findMeetingSlot(
	topic string,
	role string,
) *Meeting {
	maxMembers := bbb.config.RoomSize

	for _, m := range bbb.meetings {
		if !strings.HasPrefix(m.MeetingName, topic) {
			continue // Meeting does not match
		}

		// For users the relevant attribute is participantCount,
		// for moderators the presence of a moderator in the room
		if role == roleUser && m.ParticipantCount < maxMembers {
			return m // we found our meeting
		}
		if role == roleModerator && m.ModeratorCount == 0 {
			return m
		}
	}

	return nil
}

// JoinMeeting: Create a username based on the role
// and join the meeting with either the attendee or moderator
// password.
func (bbb *BigBlueButton) joinMeeting(
	meeting *Meeting,
	role string,
) string {
	username := meeting.NextUsername(role)
	password := meeting.Password(role)

	return bbb.client.JoinMeetingURL(
		meeting.MeetingID, username, password)
}

// DispatchClient will find a room with a free slot and will
// return the redirect url for the browser.
// If all rooms are full an overflow room will be created.
func (bbb *BigBlueButton) DispatchClient(
	topic string,
	role string,
	joinURL string,
) (string, error) {
	// For now we use a rather conservative locking
	bbb.meetingsMux.Lock()
	defer bbb.meetingsMux.Unlock()

	var (
		meeting *Meeting
		err     error
	)

	meeting = bbb.findMeetingSlot(topic, role)
	if meeting == nil {
		// Everything is full, let's create a new meeting
		meeting, err = bbb.client.CreateMeeting(topic, joinURL)
		if err != nil {
			return "", err // Well that's unfortunate.
		}

		// Add the new meeting to our state
		bbb.meetings = append(bbb.meetings, meeting)
	}
	// In case our meeting could still not be accuired,
	// we should fail and try again later.
	if meeting == nil {
		return "", fmt.Errorf("Could not get new meeting")
	}

	// Add the user to the meeting and get the redirect URL.
	// We are incrementing the participant count here
	meeting.Mux.Lock()
	defer meeting.Mux.Unlock()

	meetingURL := bbb.joinMeeting(meeting, role)

	if role == roleUser {
		meeting.ParticipantCount++
	} else {
		meeting.ModeratorCount++
	}

	// Touch sync state
	meeting.SyncedAt = time.Now()

	return meetingURL, nil
}
